using Aforo255.Cross.Discovery.Consul;
using Aforo255.Cross.Discovery.Fabio;
using Aforo255.Cross.Log.Src.Elastic;
using Aforo255.Cross.Metric.Metrics;
using Aforo255.Cross.Metric.Registry;
using Aforo255.Cross.Token.Src;
using Aforo255.Cross.Tracing.Src.Zipkin;
using Microsoft.EntityFrameworkCore;
using MS.AFORO255.Security.Data;
using MS.AFORO255.Security.Persistences;
using MS.AFORO255.Security.Services;
using Serilog;

var builder = WebApplication.CreateBuilder(args);

// configuración Nacos
builder.Host.ConfigureAppConfiguration((host, builder) =>
{
    var c = builder.Build();
    builder.AddNacosConfiguration(c.GetSection("nacosConfig"));
});

builder.WebHost.UseAppMetrics();

var config = builder.Configuration;

ExtensionsElastic.ConfigureLog(builder.Configuration);
builder.WebHost.UseSerilog();

// Add services to the container.
builder.Services.AddControllers();

builder.Services.AddDbContext<ContextDatabase>(
    opt =>
    {
        opt.UseMySQL(builder.Configuration["cn:mysql"]);
    });
builder.Services.AddScoped<IAccessService, AccessService>();
builder.Services.Configure<JwtOptions>(builder.Configuration.GetSection("jwt"));

builder.Services.AddConsul();
builder.Services.AddFabio();

builder.Services.AddJZipkin();

builder.Services.AddTransient<IMetricsRegistry, MetricsRegistry>();

var app = builder.Build();

// Configure the HTTP request pipeline.
app.UseAuthorization();
app.MapControllers();
app.UseConsul();

DbCreated.CreateDbIfNotExists(app);
app.Run();